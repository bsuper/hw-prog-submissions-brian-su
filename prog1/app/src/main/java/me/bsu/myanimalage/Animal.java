package me.bsu.myanimalage;

public class Animal {
    private String name;
    private double ageRatioToHumanYear;
    private int img;

    public Animal(String name, double ageRatioToHumanYear, int img) {
        this.name = name;
        this.ageRatioToHumanYear = ageRatioToHumanYear;
        this.img = img;
    }

    public double convertFrom(Animal from, double fromAge) {
        return fromAge / from.getAgeRatioToHumanYear() * this.ageRatioToHumanYear;
    }

    public String getConversion(Animal from) {
        return String.format("%.2f Years / %s Year", ageRatioToHumanYear / from.getAgeRatioToHumanYear(), from.getName());
    }

    public String getName() {
        return name;
    }

    public double getAgeRatioToHumanYear() {
        return ageRatioToHumanYear;
    }

    public int getImageSource() {
        return img;
    }

    public String toString() {
        return String.format("%.1f", getAgeRatioToHumanYear());
    }
}
