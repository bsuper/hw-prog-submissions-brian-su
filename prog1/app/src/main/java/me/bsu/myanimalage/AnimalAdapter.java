package me.bsu.myanimalage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.ViewHolder> {
    private ArrayList<Animal> mAnimals;
    private Animal mCurrentAnimal;
    private double mCurrentAge;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public TextView name, age, conversion;
        public ImageView img;
        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.list_item_animal_name);
            age = (TextView) v.findViewById(R.id.list_item_animal_age);
            conversion = (TextView) v.findViewById(R.id.list_item_animal_conversion);
            img = (ImageView) v.findViewById(R.id.list_item_animal_img);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AnimalAdapter(ArrayList<Animal> myDataset, Animal currentAnimal, int age) {
        mAnimals = (ArrayList<Animal>) myDataset.clone();
        mCurrentAnimal = currentAnimal;
        mCurrentAge = age;
        removeCurrentAnimal(mAnimals, currentAnimal);
    }

    private void removeCurrentAnimal(ArrayList<Animal> animals, Animal currentAnimal) {
        int indexToRemove = -1;
        for (int i = 0; i < animals.size(); i++) {
            if (currentAnimal.getName().equals(animals.get(i).getName())) {
                indexToRemove = i;
            }
        }
        if (indexToRemove >= 0) {
            animals.remove(indexToRemove);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AnimalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_animal, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Animal a = mAnimals.get(position);
        holder.name.setText(a.getName());
        holder.conversion.setText(String.format("%s", a.getConversion(mCurrentAnimal)));
        holder.age.setText(String.format("%.1f", a.convertFrom(mCurrentAnimal, mCurrentAge)));
        holder.img.setImageResource(a.getImageSource());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mAnimals.size();
    }
}