package me.bsu.myanimalage;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    AnimalAdapter mAnimalAdapter;
    ArrayList<Animal> mAnimals;
    Animal mCurrentAnimal;
    int mCurrentAge;

    View mRootView;
    RecyclerView mRecyclerView;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView =  inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.listview_animal);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(
                getActivity()
        ));

        mAnimals = defaultAnimals();

        if (savedInstanceState != null) {
            mCurrentAge = savedInstanceState.getInt("currAge", 1);
            mCurrentAnimal = mAnimals.get(indexOfAnimal(savedInstanceState.getString("currAnimal", "Human"), mAnimals));
        } else {
            mCurrentAnimal = mAnimals.get(13);
            mCurrentAge = 1;
        }

        mAnimalAdapter = new AnimalAdapter(mAnimals, mCurrentAnimal, mCurrentAge);


        mRecyclerView.setAdapter(mAnimalAdapter);
        loadCurrentAnimal(mRootView, mCurrentAnimal, mCurrentAge);
        setupChangeButton(mRootView);
        return mRootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("currAnimal", mCurrentAnimal.getName());
        outState.putInt("currAge", mCurrentAge);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentAge = savedInstanceState.getInt("currAge", 1);
            mCurrentAnimal = mAnimals.get(indexOfAnimal(savedInstanceState.getString("currAnimal", "Human"), mAnimals));
        }
    }

    private void setupChangeButton(View rootView) {
        Button changeButton = (Button) rootView.findViewById(R.id.change_current_animal_button);
        changeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Animal currentSelection = mCurrentAnimal;
                double age = mCurrentAge;

                boolean wrapInScrollView = true;
                final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .title(R.string.change_current_animal_title)
                        .customView(R.layout.change_animal, wrapInScrollView)
                        .theme(Theme.LIGHT)
                        .positiveText(R.string.change)
                        .negativeText(R.string.cancel).show();

                View customView = dialog.getCustomView();
                final TextView currentAnimalSelection = (TextView) customView.findViewById(R.id.change_animal_name_text_view);
                final SeekBar currentAnimalAgeSeekBar = (SeekBar) customView.findViewById(R.id.change_animal_age_bar);
                final EditText currentAnimalAgeEditText = (EditText) customView.findViewById(R.id.change_animal_age_edit_text);
                currentAnimalSelection.setText(currentSelection.getName());

                View changeAnimalView = customView.findViewById(R.id.change_animal_clickable);

                changeAnimalView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MaterialDialog.Builder(getActivity())
                                .items(animalNames(mAnimals))
                                .theme(Theme.LIGHT)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                        currentAnimalSelection.setText(mAnimals.get(which).getName());
                                    }
                                })
                                .show();
                    }
                });

                currentAnimalAgeEditText.setText(String.format("%.1f", age));
                currentAnimalAgeSeekBar.setProgress((int) age);

                currentAnimalAgeEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            int age = getAgeFromChangeAgeEditText(currentAnimalAgeEditText);
                            currentAnimalAgeSeekBar.setProgress((int) age);
                            return false;
                        }
                        return false;
                    }

                });

                currentAnimalAgeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        currentAnimalAgeEditText.setText(String.format("%d", progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

                dialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCurrentAnimal = mAnimals.get(indexOfAnimal(currentAnimalSelection.getText().toString(), mAnimals));
                        int age = getAgeFromChangeAgeEditText(currentAnimalAgeEditText);
                        currentAnimalAgeSeekBar.setProgress((int) age);
                        mCurrentAge = age;
                        updateCurrentAnimal();
                        dialog.dismiss();
                    }
                });

            }
        });
    }

    private int getAgeFromChangeAgeEditText(EditText currentAnimalAgeEditText) {
        int age = 0;
        try {
            age = (int) Math.round(Double.parseDouble(currentAnimalAgeEditText.getText().toString()));
        } catch (NumberFormatException e) {
        }
        return age;
    }

    private void updateCurrentAnimal() {
        loadCurrentAnimal(mRootView, mCurrentAnimal, mCurrentAge);
        mAnimalAdapter = new AnimalAdapter(mAnimals, mCurrentAnimal, mCurrentAge);
        mRecyclerView.setAdapter(mAnimalAdapter);
    }

    private int indexOfAnimal(String animalName, ArrayList<Animal> animals) {
        for (int i = 0; i < mAnimals.size(); i++) {
            if (animalName.equals(mAnimals.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }

    private void loadCurrentAnimal(View rootView, Animal a, int currentAge) {
        ImageView currentAnimalImageView = (ImageView) rootView.findViewById(R.id.current_animal_img);
        currentAnimalImageView.setImageResource(a.getImageSource());

        TextView currentAnimalNameTextView = (TextView) rootView.findViewById(R.id.current_animal_name);
        currentAnimalNameTextView.setText(a.getName());

        TextView currentAnimalAgeTextView = (TextView) rootView.findViewById(R.id.current_animal_age);
        currentAnimalAgeTextView.setText(String.format("%d Years Old", mCurrentAge));
    }

    private ArrayList<Animal> defaultAnimals() {
        Animal[] a = new Animal[] {
                new Animal("Bear (Oski)", 2, R.drawable.bear13),
                new Animal("Cat", 3.2, R.drawable.cat19),
                new Animal("Chicken", 5.33, R.drawable.egg15),
                new Animal("Cow", 3.64, R.drawable.cow9),
                new Animal("Deer", 2.29, R.drawable.deer2),
                new Animal("Dog", 3.64, R.drawable.dog77),
                new Animal("Deer", 4.21, R.drawable.duck10),
                new Animal("Elephant", 1.14, R.drawable.elephant6),
                new Animal("Fox", 5.71, R.drawable.fox1),
                new Animal("Goat", 5.33, R.drawable.capricorn2),
                new Animal("Hamster", 20, R.drawable.animal86),
                new Animal("Hippopotamus", 1.78, R.drawable.animal90),
                new Animal("Horse", 2, R.drawable.horse125),
                new Animal("Human", 1, R.drawable.man23),
                new Animal("Kangaroo", 8.89, R.drawable.kangaroo),
                new Animal("Lion", 2.29, R.drawable.leo5),
                new Animal("Monkey", 3.2, R.drawable.monkey3),
                new Animal("Mouse", 20, R.drawable.mouse33),
                new Animal("Pig", 3.2, R.drawable.pig4),
                new Animal("Pigeon", 7.27, R.drawable.peace7),
                new Animal("Rabbit", 8.89, R.drawable.rabbit5),
                new Animal("Rat", 26.67, R.drawable.rat2),
                new Animal("Sheep", 5.33, R.drawable.lamb1),
                new Animal("Squirrel", 5, R.drawable.squirrel3),
                new Animal("Wolf", 4.44, R.drawable.wolves)
        };

        return new ArrayList<Animal>(Arrays.asList(a));
    }

    private String[] animalNames(ArrayList<Animal> animals) {
        String[] ret = new String[animals.size()];
        for (int i = 0; i < animals.size(); i++) {
            ret[i] = animals.get(i).getName();
        }
        return ret;
    }
}
